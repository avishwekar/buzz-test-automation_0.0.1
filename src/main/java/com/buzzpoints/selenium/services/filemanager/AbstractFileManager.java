package com.buzzpoints.selenium.services.filemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlittman on 12/7/15.
 */
public abstract class AbstractFileManager {
    private List<File> pendingFiles = new ArrayList<File>();

    /**
     * Abstract method stub for uploading files as necessary
     */
    public abstract void uploadFiles();

    /**
     *
     * @return
     */
    public List<File> getFiles() {
        return pendingFiles;
    }

    public void addFile(File file) {
        if(!pendingFiles.contains(file))
            pendingFiles.add(file);
    }

    public void addFiles(List<File> files){
        for(File file : files) {
            addFile(file);
        }
    }

}
