package com.buzzpoints.selenium.main;

import java.util.ArrayList;
import java.util.List;
 
import org.testng.TestNG;
 
public class BuzzTestAutomationMain {
 
public static void main(String[] args) {
 
// object of TestNG Class
TestNG runner=new TestNG();
 
// list of String 
List<String> suitefiles=new ArrayList<String>();
 
// xml file which you have to execute
suitefiles.add("src/main/resource/selenium.xml");
 
// now set xml file for execution
runner.setTestSuites(suitefiles);
 
// execute the runner using run method
runner.run();
}
 
}