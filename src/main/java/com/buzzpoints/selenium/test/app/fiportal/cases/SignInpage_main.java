package com.buzzpoints.selenium.test.app.fiportal.cases;

import org.testng.annotations.Test;

import com.buzzpoints.selenium.api.GenericHelper;
import com.buzzpoints.selenium.api.StartWebDriver;
import com.buzzpoints.selenium.api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/*
 * Sign in the page using valid username and
 * password
 */

public class SignInpage_main extends StartWebDriver {
	
	@Test
	public void testSignInpage_main() throws Exception{
		try {
		//Enter your username and password and click on the Sign In button  
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //	Go to the sign in page, enter an incorrect username and a correct password and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadencee");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //Go to the sign in page, enter an incorrect password and a correct username and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //Enter an incorrect username and incorrect password and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincaaadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaaa11");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
		System.out.println("Test Passed :++++++++++"+this.getClass()+"++++++++++");

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			System.out.println("Test Failed :++++++++++"+this.getClass()+"++++++++++");

			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}
	}
	
	public boolean securityQuestion() {
		if(driver.findElement(By.xpath("//span[text()='What was your childhood nickname?']")) != null) 
			return true;
		else
			return false;
	}

}