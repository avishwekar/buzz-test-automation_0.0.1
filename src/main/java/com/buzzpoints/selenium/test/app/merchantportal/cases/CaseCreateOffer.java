package com.buzzpoints.selenium.test.app.merchantportal.cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.buzzpoints.selenium.api.TestCase;
import com.buzzpoints.selenium.api.TestConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmata on 02/11/16.
 */
@Deprecated
public class CaseCreateOffer extends TestCase {

	/**
	 * Creates a CreateOffer
	 * @param cfg
	 */
	public CaseCreateOffer(TestConfig cfg) {
	}

	/**
	 * Run the Create Offer test case
	 * @param driver the current WebDriver instance
	 * @return True if the offer was created
	 */
	public Results run(WebDriver driver) {
		List<Result> resultList = new ArrayList<Result>();
		Results results = new Results(this, driver, resultList);

		try {
			WebElement navbar = waitUntilExists(driver, By.cssSelector("nav.navbar"), 10l, 500l);
			if (navbar == null)
				throw new Exception("Could not locate the nav bar.");

			WebElement campaignDropdown = navbar.findElement(By.xpath("//a[contains(.,'Campaigns')]"));
			if (campaignDropdown == null)
				throw new Exception("Could not locate Campaigns dropdown inside the nav bar.");
			mouseClick(driver, campaignDropdown);

			WebElement offersLink = navbar.findElement(By.cssSelector("a[href=\"#/portal/campaigns/offers\"]"));
			if (offersLink == null)
				throw new Exception("Could not locate Offers link inside the Campaigns dropdown.");
			offersLink.click();

			if (waitUntilURLContains(driver, "#/portal/campaigns/offers", 10, 500) == false)
				throw new Exception("Could not navigate to Offers page.");

			WebElement createOfferButton = waitUntilExists(driver, By.xpath("//a[contains(.,'Create an Offer')] | //button[contains(.,'Create an Offer')]"), 25, 250);
			if (createOfferButton == null)
				throw new Exception("Could not find Create an Offer button.");
			mouseClick(driver, createOfferButton);

			WebElement suggestedButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Use a Suggested Amount')]"), 25, 250);
			if (createOfferButton == null)
				throw new Exception("Could not find Suggested Amount button.");
			suggestedButton.click();

			WebElement suggestedAmount = waitUntilExists(driver, By.xpath("//select/option[@value='5% Off']"), 25, 250);
			if (suggestedAmount == null)
				throw new Exception("Could not find amount in dropdown.");
			suggestedAmount.click();

			WebElement offerText = waitUntilExists(driver, By.xpath("//input[@type='text']"), 5, 150);
			if (offerText == null)
				throw new Exception("Could not find text field.");
			offerText.sendKeys("Reality");

			WebElement nextButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Next')]"), 5, 150);
			if (nextButton == null)
				throw new Exception("Could not find next button.");
			mouseClick(driver, nextButton);

			WebElement pickDate = waitUntilExists(driver, By.xpath("html/body/div[1]/div/div/div/div[1]/div[2]/div/div[1]/div[1]/div[2]/div/form/div[2]/div/table/tbody/tr/td/button"), 5, 150);
			if (pickDate == null)
				throw new Exception("Could not pick a date.");
			pickDate.click();

			WebElement nextButton2 = waitUntilExists(driver, By.xpath("//button[contains(.,'Next')]"), 5, 150);
			if (nextButton2 == null)
				throw new Exception("Could not find next button2.");
			mouseClick(driver, nextButton2);

			WebElement nextButton3 = waitUntilExists(driver, By.xpath("//button[contains(.,'Next')]"), 5, 150);
			if (nextButton3 == null)
				throw new Exception("Could not find next button3.");
			mouseClick(driver, nextButton3);

//			WebElement createButton = waitUntilExists(driver, By.xpath("html/body/div[1]/div/div/div/div[1]/div[2]/div/div[1]/div[2]/button[2]"), 10, 150);
			WebElement createButton = waitUntilExists(driver, By.xpath("//*[contains(.,'Create')]"), 10, 150);
			if (createButton == null)
				throw new Exception("Could not find create button.");
			mouseClick(driver, createButton);
			onSuccess(resultList);
			return results.done(true);
		} catch (Exception e) {
			results.error(e, e.getMessage());
		}

		return results.done(false);
	}
}
