package com.buzzpoints.selenium.test.app.merchantportal.cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.buzzpoints.selenium.api.TestCase;
import com.buzzpoints.selenium.api.TestConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmata on 02/17/16.
 */
@Deprecated
public class CaseCreatePointCampaign extends TestCase {

	/**
	 * Creates a CaseCreatePointCampaign
	 * @param cfg
	 */

	public CaseCreatePointCampaign(TestConfig cfg) {
	}

	/**
	 * Run the Create Point Campaign test case
	 * @param driver the current WebDriver instance
	 * @return True if the offer was created
	 */
	public Results run(WebDriver driver) {
		List<Result> resultList = new ArrayList<Result>();
		Results results = new Results(this, driver, resultList);

		try {
			WebElement navbar = waitUntilExists(driver, By.cssSelector("nav.navbar"), 10l, 500l);
			if (navbar == null)
				throw new Exception("Could not locate the nav bar.");

			WebElement campaignDropdown = navbar.findElement(By.xpath("//a[contains(.,'Campaigns')]"));
			if (campaignDropdown == null)
				throw new Exception("Could not locate Campaigns dropdown inside the nav bar.");
			mouseClick(driver, campaignDropdown);

			WebElement pointsLink = navbar.findElement(By.cssSelector("a[href=\"#/portal/campaigns/points\"]"));
			if (pointsLink == null)
				throw new Exception("Could not locate Points link inside the Campaigns dropdown.");
			pointsLink.click();

			if (waitUntilURLContains(driver, "#/portal/campaigns/points", 10, 500) == false)
				throw new Exception("Could not navigate to point campaign page.");

			WebElement createPointButton = waitUntilExists(driver, By.xpath("//*[contains(.,'a Point Campaign')]"), 25, 250);
			if (createPointButton == null)
				throw new Exception("Could not find Create a Point Campaign button.");
			mouseClick(driver, createPointButton);

//			WebElement nextButton = waitUntilExists(driver, By.className("btn.btn-primary.pull-right.ng-scope"), 25, 150);
//			WebElement nextButton = waitUntilExists(driver, By.xpath("//input[@type='submit']"), 35, 150);
//			WebElement nextButton = waitUntilExists(driver, By.xpath("//input[contains(.,'Next')]"), 25, 150);
			WebElement nextButton = waitUntilExists(driver, By.className("btn"), 25, 150);
			if (nextButton == null)
				throw new Exception("Could not find next button.");
			mouseClick(driver, nextButton);
			System.out.println("Clicked on the first Next button");

//			waitUntilExists(driver, By.xpath("//*[contains(.,'Campaign Duration')]"), 20, 150);
//			System.out.println("Found Campaign Duration");
			
			WebElement checkBox = waitUntilExists(driver, By.xpath("//*[contains(.,'Ongoing campaign')]"), 20, 150);
//			WebElement checkBox = waitUntilExists(driver, By.className("form-group.checkbox.ng-valid"), 20, 150);
			if (checkBox == null)
				throw new Exception("Could not find checkbox.");
			checkBox.click();

			WebElement nextButton2 = waitUntilExists(driver, By.className("btn-primary"), 25, 150);
			if (nextButton2 == null)
				throw new Exception("Could not find next button2.");
			mouseClick(driver, nextButton2);

			waitUntilExists(driver, By.xpath("//*[contains(.,'Availability')]"), 20, 150);
			System.out.println("Found Availability page");

//			WebElement nextButton3 = waitUntilExists(driver, By.xpath("//*[contains(.,'Next')]"), 25, 150);
//			WebElement nextButton3 = waitUntilExists(driver, By.xpath("//*[@value='Next']"), 25, 150);
			WebElement nextButton3 = waitUntilExists(driver, By.xpath("//*[contains(@value,'Next']"), 25, 150);
//			WebElement nextButton3 = waitUntilExists(driver, By.xpath("html/body/div[1]/div/div/div/div[1]/div/div/div[1]/div/div[2]/section[3]/input[2]"), 25, 150);
			if (nextButton3 == null)
				throw new Exception("Could not find next button3.");
			mouseClick(driver, nextButton3);

			WebElement createButton = waitUntilExists(driver, By.className("btn-primary"), 20, 150);
			if (createButton == null)
				throw new Exception("Could not find create button.");
			mouseClick(driver, createButton);
			System.out.println("Clicked on the Create button");
			
			waitUntilExists(driver, By.xpath("//*[contains(.,'Thanks for Creating')]"), 20, 150);
			System.out.println("Found Thanks page");
			WebElement doneButton = waitUntilExists(driver, By.xpath("//*[contains(.,'Done')]"), 10, 150);
			if (doneButton == null)
				throw new Exception("Could not find done button.");
			mouseClick(driver, createButton);
			System.out.println("Clicked on the Done button");

			onSuccess(resultList);
			return results.done(true);
		} catch (Exception e) {
			results.error(e, e.getMessage());
		}

		return results.done(false);
	}
}
