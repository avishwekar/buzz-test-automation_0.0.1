package com.buzzpoints.selenium.test.app.fiportal.cases;

import org.testng.annotations.Test;

import com.buzzpoints.selenium.api.GenericHelper;
import com.buzzpoints.selenium.api.StartWebDriver;
import com.buzzpoints.selenium.api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/*
 * Sign in the page using valid username and
 * password
 */

public class SignInpage_forgotpassword extends StartWebDriver {
	
	@Test
	public void testSignInpage_forgotpassword() throws Exception{
		try {
		driver.get("http://hetzner.buzzpoints.com:3010/");
		driver.findElement(By.linkText("Forgot password?")).click();
//	    driver.findElement(By.cssSelector("input.form-control")).clear();
	    driver.findElement(By.xpath("(//input)[1]")).sendKeys("analystcadence");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(2000);
//	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).clear();
	    try {
			if(driver.findElement(By.xpath("//span[text()='What was your childhood nickname?']")) != null)
				TextBoxHelper.typeInTextBox("(//input[@type='text'])[2]", "montu");
			} catch(Exception e) {
				TextBoxHelper.typeInTextBox("(//input[@type='text'])[2]", "aditya");
			}

	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();Thread.sleep(2000);
//	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Aaaaaaa1");Thread.sleep(2000);
	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();Thread.sleep(2000);
	    driver.findElement(By.linkText("Forgot password?")).click();
	    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(2000);
//	    driver.findElement(By.cssSelector("input.form-control")).clear();
	    driver.findElement(By.xpath("(//input)[1]")).sendKeys("analystcadence");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(2000);
//	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).sendKeys("adi");
	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();Thread.sleep(2000);
	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).sendKeys("Aditya");Thread.sleep(2000);
	    driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();Thread.sleep(2000);
////	    driver.findElement(By.xpath("//input[@type='submit']")).clear();
//	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).clear();
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Aaaaaaa1");
//	    driver.findElement(By.xpath("//input[@type='password']")).clear();
//	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa");
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).clear();
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Aaaaaaa");
//	    driver.findElement(By.xpath("//input[@type='password']")).clear();
//	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).clear();
//	    driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Aaaaaaa1");
//	    driver.findElement(By.xpath("(//button[@type='submit'])[3]")).click();Thread.sleep(2000);
		System.out.println("Test Passed :++++++++++"+this.getClass()+"++++++++++");

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			System.out.println("Test Failed :++++++++++"+this.getClass()+"++++++++++");

			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}
	}
	
	public boolean securityQuestion() {
		if(driver.findElement(By.xpath("//span[text()='What was your childhood nickname?']")) != null) 
			return true;
		else
			return false;
	}

}