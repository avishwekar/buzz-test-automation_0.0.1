package com.buzzpoints.selenium.test.app.merchantportal.cases;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.buzzpoints.selenium.api.TestCase;
import com.buzzpoints.selenium.api.TestConfig;

/**
 * 
 * @author bloviat
 *
 */
@Deprecated
public class CaseEndReward extends TestCase {

	final int amount;

	/**
	 * Ends a Reward
	 * 
	 * @param cfg
	 */
	public CaseEndReward(TestConfig cfg) {
		amount = (Integer) getConfig(cfg, "reward-amount-other");
//		System.out.println("first check point");
	}
	
	/***
	 * Run the End Reward test case
	 * 
	 * @param driver
	 *            of the current WebDriver instance
	 * @return True if the reward was ended
	 */
	@Override
	public Results run(WebDriver driver) throws InterruptedException {
		List<Result> resultList = new ArrayList<Result>();
		Results results = new Results(this, driver, resultList);
		try {
			WebElement navbar = waitUntilExists(driver, By.cssSelector("nav.navbar"), 10l, 500l);
			if (navbar == null)
				throw new Exception("Could not locate the nav bar.");

			WebElement campaignDropdown = navbar.findElement(By.xpath("//a[contains(.,'Campaigns')]"));
			if (campaignDropdown == null)
				throw new Exception("Could not locate Campaigns dropdown inside the nav bar.");

			mouseClick(driver, campaignDropdown);
			WebElement rewardsLink = navbar.findElement(By.cssSelector("a[href=\"#/portal/campaigns/rewards\"]"));
			if (rewardsLink == null)
				throw new Exception("Could not locate Rewards link inside the Campaigns dropdown.");

			rewardsLink.click();

			if (waitUntilURLContains(driver, "#/portal/campaigns/rewards", 10, 500) == false)
				throw new Exception("Could not navigate to rewards page.");

			WebElement endRewardButton = waitUntilExists(driver, By.xpath("//button[contains(.,'End Reward')]"), 25, 250);
			if (endRewardButton == null)
				throw new Exception("Cannot find End Reward button.");

			String tooltip = endRewardButton.getAttribute("tooltip");
			if (tooltip.indexOf("You must have at least one reward") != -1) { // Only one reward available, shouldn't be able to end it
				createSecondReward(driver, resultList, results); // Add another reward in order to end one
				driver.navigate().refresh();
				endRewardButton = waitUntilExists(driver, By.xpath("//button[contains(.,'End Reward')]"), 25, 250);
			}
			if (endRewardButton == null)
				throw new Exception("Could not find End Reward button.");

			mouseClick(driver, endRewardButton);

			WebElement okEndRewardButton = waitUntilExists(driver, By.xpath("//button[contains(@class,'confirm')]"), 20, 250);
			if (okEndRewardButton == null)
				throw new Exception("Could not find OK button to end the reward.");

			mouseClick(driver, okEndRewardButton);

			//driver.navigate().refresh();

			if (waitUntilNotExists(driver,
					By.xpath("//div[contains(@class,'campaign-ereward-container')]//div[contains(@class, 'campaign-active-ereward-header')]//span[contains(.,'$" + String.valueOf(amount) + ".00')]"),
					80, 250) == false)
				throw new Exception("Unable to verify successful completion of ending a reward.");

			onSuccess(resultList);
			return results.done(true);
		} catch (Exception e) {
			results.error(e, e.getMessage());
		}
		return results.done(false);
	}

	protected void createSecondReward(WebDriver driver, List<Result> resultList, Results results) throws Exception {
		WebElement createRewardButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Create a Reward')]"), 20, 250);
		if (createRewardButton == null)
			throw new Exception("Could not find Create a Reward button");

		mouseClick(driver, createRewardButton);
		WebElement rewardButton = findElement(driver, By.cssSelector("input[ng-value=\"" + String.valueOf(amount) + "\"]"));
		if (rewardButton == null)
			throw new Exception("Could not locate reward button matching amount '" + String.valueOf(amount) + "'.");

		rewardButton.click();

		WebElement nextButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Next')]"), 5, 150);
		if (nextButton == null)
			throw new Exception("Could not find next button.");

		mouseClick(driver, nextButton);

		WebElement createButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Create')]"), 5, 150);
		if (createButton == null)
			throw new Exception("Could not find create button.");

		mouseClick(driver, createButton);
	}

}
