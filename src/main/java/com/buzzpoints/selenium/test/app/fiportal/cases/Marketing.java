package com.buzzpoints.selenium.test.app.fiportal.cases;

import org.testng.annotations.Test;

import com.buzzpoints.selenium.api.ButtonHelper;
import com.buzzpoints.selenium.api.GenericHelper;
import com.buzzpoints.selenium.api.LinkHelper;
import com.buzzpoints.selenium.api.StartWebDriver;
import com.buzzpoints.selenium.api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;



import org.testng.AssertJUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;


public class Marketing extends StartWebDriver {
	
	@Test
	public void testMarketing()  {

				
		try {
			// SR Level1 user  
			driver.get("http://hetzner.buzzpoints.com:3010/");
			driver.findElement(By.id("usernameSignIn")).clear();
			driver.findElement(By.id("usernameSignIn")).sendKeys("srlevel1");
			driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.cssSelector("a[href*='http://learn.buzzpoints.com/']")).click();
		    LinkHelper.clickLink("Marketing");
		    driver.findElement(By.cssSelector("a.dropdown-toggle.userSelectBtn")).click();
		    driver.findElement(By.linkText("Sign Out")).click();
		    
		    //SR Level2 user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
			driver.findElement(By.id("usernameSignIn")).clear();
			driver.findElement(By.id("usernameSignIn")).sendKeys("srlevel2");
			driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();Thread.sleep(1500); 
		    LinkHelper.clickLink("Marketing");
		    driver.findElement(By.cssSelector("a.dropdown-toggle.userSelectBtn")).click();
		    driver.findElement(By.linkText("Sign Out")).click();
		    
		    //FI manager user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("fimgrcadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();Thread.sleep(1500); 
		    LinkHelper.clickLink("Marketing");
		    driver.findElement(By.cssSelector("a.dropdown-toggle.userSelectBtn")).click();
		    driver.findElement(By.linkText("Sign Out")).click();
		    
		    //FI Reports Viewer user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("analystcadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();Thread.sleep(1500);  
		    LinkHelper.clickLink("Marketing");
		    driver.findElement(By.cssSelector("a.dropdown-toggle.userSelectBtn")).click();
		    driver.findElement(By.linkText("Sign Out")).click();
		    
		    //Buzz Admin user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("buzzadmincadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();Thread.sleep(1500);
		    LinkHelper.clickLink("Marketing");
		    driver.findElement(By.cssSelector("a.dropdown-toggle.userSelectBtn")).click();
		    driver.findElement(By.linkText("Sign Out")).click();

		System.out.println("Test Passed :++++++++++"+this.getClass()+"++++++++++");

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			System.out.println("Test Failed :++++++++++"+this.getClass()+"++++++++++");

			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}
		
		
	}

}
